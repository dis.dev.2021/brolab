import 'focus-visible';
import lazyImages from './plugins/lazyImages';
import documentReady from './helpers/documentReady';
import modal from './components/modal';
// import Choices from 'choices.js';
// import {WebpMachine} from "webp-hero"
import Swiper, {Autoplay, Navigation, Pagination, Thumbs} from 'swiper';
Swiper.use([Navigation, Pagination, Autoplay, Thumbs]);

documentReady(() => {
   lazyImages();
   modal();


    let technology = new Swiper(".technology-slider", {
        slidesPerView: 3,
        spaceBetween: 0,
        slidesPerGroup: 3,
        loop: true,
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        breakpoints: {
            768: {
                slidesPerView: 3,
                slidesPerGroup: 3,
            },
            1230: {
                slidesPerView: 4,
                slidesPerGroup: 4,
            },
            1450: {
                slidesPerView: 5,
                slidesPerGroup: 5,
            },
        },
    });

    let comments_mobile = new Swiper(".comments-mobile-slider", {
        slidesPerView: 1,
        spaceBetween: 40,
        loop: true,
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        breakpoints: {
            768: {
                slidesPerView: 2,
                slidesPerGroup: 2
            },
            1024: {
                slidesPerView: 2,
                slidesPerGroup: 2,
                spaceBetween: 80,
            }
        },
    });

    let commentsDesktop= new Swiper(".comments-desktop-slider", {
        slidesPerView: 1,
        spaceBetween: 50,
        loop: true,
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        }
    });

});


